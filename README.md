# Custom Inputs - ReactJS

This repository contains a little inputs, made in ReactJS, to help in my works.

## Dependencies

After clone this, just run:

```bash
npm install --save-dev tachyons@4.9.0
npm install --save tachyons tachyons-cli
npm install jquery jquery-mask-plugin ramda
```
**Use the flag '-g' to install globally**

Any doubt about tachyons, please read [this document](https://github.com/tachyons-css/tachyons-and-react/tree/master/getting-started)