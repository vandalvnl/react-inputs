import React from 'react';
import './stylesheet.css';
import { replace } from 'ramda';
import 'jquery-mask-plugin';
import $ from 'jquery';

export default class NormalInput extends React.Component {
	constructor(props) {
		super();
		this.state = {
			id: props.id,
			placeholder: props.placeholder,
			mask: props.mask,
			class: props.className + ' normal-input',
			msgErro: props.msgErro,
			errorOn: 'none',
			value: props.value,
			isError: props.isError
		};
		this.handleChange = this.handleChange.bind(this);
		this.validate = this.validate.bind(this);
		this.capitalize = this.capitalize.bind(this);
	}

	setMaskRegex() {
		if (this.state.mask === 'email') {
			$('#' + this.state.id).mask('A', {
				translation: {
					A: { pattern: /[\w@\-.+]/, recursive: true }
				}
			});
		} else if (this.state.mask === 'cellphone') {
			$('#' + this.state.id).mask('(00) 00000-0000');
		} else if (this.state.mask === 'money') {
			$('#' + this.state.id).mask('000.000.000.000.000,00', { reverse: true });
		} else if (this.state.mask === 'date') {
			$('#' + this.state.id)
				.mask('AB/CD/0000', {
					translation: {
						A: { pattern: /[0-3]/ },
						B: { pattern: /[0-9]/ },
						C: { pattern: /[0-1]/ },
						D: { pattern: /[0-9]/ }
					}, onKeyPress: function (a, b, c, d) {
						if (!a) return;
						let m = a.match(/(\d{1})/g);
						if (!m) return;
						if (parseInt(m[0]) === 3) {
							d.translation.B.pattern = /[0-1]/;
						} else {
							d.translation.B.pattern = /[0-9]/;
						}
						if (parseInt(m[2]) === 1) {
							d.translation.D.pattern = /[0-2]/;
						} else {
							d.translation.D.pattern = /[0-9]/;
						}
						let temp_value = c.val();
						c.val('');
						c.unmask().mask('AB/CD/0000', d);
						c.val(temp_value);
					}
				}).keyup();
		} else if (this.state.mask === 'cpf') {
			$('#' + this.state.id).mask('000.000.000-00', { reverse: true });
		} else if (this.state.mask === 'cnpj') {
			$('#' + this.state.id).mask('00.000.000/0000-00', { reverse: true });
		} else if (this.state.mask === 'time') {
			$('#' + this.state.id).mask('00:00:00');
		} else if (this.state.mask === 'cep') {
			$('#' + this.state.id).mask('00000-000');
		}
	}

	capitalize(string) {
		var pieces = string.split(' ');
		for (var i = 0; i < pieces.length; i++) {
			var j = pieces[i].charAt(0).toUpperCase();
			pieces[i] = j + pieces[i].substr(1);
		}
		return pieces.join(' ');
	}

	handleChange(event) {
		if (this.state.mask === 'name') {
			this.setState({ value: this.capitalize(replace(/[0-9_[]]+/, '', event.target.value)) });
		}
		this.setState({ value: event.target.value });
	}

	validate() {
		this.setState((prevState) => ({ isError: !prevState.isError }));
		if (this.state.isError) {
			this.setState({ class: this.state.class + ' normal-input-error' });
			this.setState({ errorOn: 'block' });
		} else {
			this.setState({ class: replace('normal-input-error', '', this.state.class) });
			this.setState({ errorOn: 'none' });
		}
	}

	componentDidMount() {
		this.setMaskRegex();
	}

	render() {
		let msgErrorVar = { display: this.state.errorOn };
		return (
			<div>
				<input
					placeholder={this.state.placeholder}
					className={this.state.class}
					id={this.state.id}
					onChange={this.handleChange}
					value={this.state.value}
				/>
				<span className="help-block" style={msgErrorVar}>
					{this.state.msgErro}
				</span>
				<button onClick={this.validate}>Send Error</button>
			</div>
		);
	}
}
