import React, { Component } from 'react';
import './css/main.css';

class BusCard extends Component {
	render() {
		return (
			<div>
				<div className="fl w-w-third pa2">
					<div className="card card--front">
						<div className="card__title">Fullano Nome Importado </div>
						<div className="card__number">309400012349876</div>
						<div className="card__expiry-date">10/17</div>
						<div className="card__owner">Info Extra</div>
						<img className="card__logo" src="http://198.199.72.106/assets/img/suportrans_big.png" />
					</div>
				</div>
			</div>
		);
	}
}

export default BusCard;
